//
//  CrearClienteRequest.swift
//  PreguntaleAlDocPaciente
//
//  Created by Osvaldo Fernández on 13/05/21.
//

import Foundation

struct CrearClienteRequest {
    let action: String
    let id_usuario: String
    let calle: String
    let colonia: String
    let ciudad: String
    let estado: String
    let codigo_postal: String
    
    var dictionaryRepresentation: [String: Any] {
           return [
            "action": action,
            "id_usuario": id_usuario,
            "calle": calle,
            "colonia": colonia,
            "ciudad": ciudad,
            "estado": estado,
            "codigo_postal": codigo_postal
           ]
       }
}
