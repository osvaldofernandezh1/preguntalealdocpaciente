//
//  ListaConsultasRequest.swift
//  PreguntaleAlDocPaciente
//
//  Created by Osvaldo Fernández on 14/05/21.
//

import Foundation

struct ListaConsultasRequest {
    let action: String
    let id_usuario: String
    
    var dictionaryRepresentation: [String: String] {
        return [
            "action": action,
            "id_usuario": id_usuario
        ]
    }
}
