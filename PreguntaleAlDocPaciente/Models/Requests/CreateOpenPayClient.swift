//
//  CreateOpenPayClient.swift
//  PreguntaleAlDocPaciente
//
//  Created by Osvaldo Fernández on 09/05/21.
//

import Foundation

struct CreateOpenPayClient: Codable {
    let name: String
    let email: String
    let requires_account: Bool
}
