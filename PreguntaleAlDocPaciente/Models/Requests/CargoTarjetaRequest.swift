//
//  CargoTarjetaRequest.swift
//  PreguntaleAlDocPaciente
//
//  Created by Osvaldo Fernández on 14/05/21.
//

import Foundation

struct CargoTarjetaRequest {
    let action: String
    let id_consulta: String
    let customerId: String
    let source_id: String //Id de la tarjeta
    let folio: String
    let amount: String
    let deviceHiddenFieldName: String
    let description: String
}
