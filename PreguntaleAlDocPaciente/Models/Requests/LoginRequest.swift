//
//  LoginRequest.swift
//  PreguntaleAlDocPaciente
//
//  Created by Osvaldo Fernández on 19/04/21.
//

import Foundation

struct LoginRequest {
    let celular_usuario: String
    let device_token: String
    let password_usuario: String
    let id_tipo_dispositivo: String
    
    var dictionaryRepresentation: [String: Any] {
           return [
            "celular_usuario": celular_usuario,
            "password_usuario": password_usuario,
            "device_token": device_token,
            "id_tipo_dispositivo": id_tipo_dispositivo
           ]
       }
}
