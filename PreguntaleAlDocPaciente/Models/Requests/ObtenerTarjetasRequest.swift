//
//  ObtenerTarjetasRequest.swift
//  PreguntaleAlDocPaciente
//
//  Created by Osvaldo Fernández on 13/05/21.
//

import Foundation

struct ObtenerTarjetasRequest {
    let action: String
    let idopenpay: String
    
    var dictionaryRepresentation: [String: Any] {
           return [
            "action": action,
            "idopenpay": idopenpay
           ]
       }
}
