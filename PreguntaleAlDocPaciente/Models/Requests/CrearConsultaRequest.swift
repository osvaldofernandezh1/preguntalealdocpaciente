//
//  CrearConsultaRequest.swift
//  PreguntaleAlDocPaciente
//
//  Created by Osvaldo Fernández on 14/05/21.
//

import Foundation

struct CrearConsultaRequest {
    let action: String
    let id_usuario: String
    let id_doctor: String
    let fecha_cita: String
    let transaccion: String
    let costo_consulta: String
    let nombre_paciente: String
    let edad: String
    let peso: String
    let motivo: String
    let medicamentos: String
    let antecedentes: String
    let id_horario: String
}
