//
//  HorariosRequest.swift
//  PreguntaleAlDocPaciente
//
//  Created by Osvaldo Fernández on 14/05/21.
//

import Foundation

struct HorariosRequest {
    let action: String
    let fecha_cita: String
    let id_doctor: String
}
