//
//  PerfilMenuOption.swift
//  PreguntaleAlDocPaciente
//
//  Created by Osvaldo Fernández on 19/04/21.
//

import Foundation

struct PerfilMenuOption {
    let image: String
    let title: String
    let navigation: String?
}
