//
//  ObtenerTarjetasResponse.swift
//  PreguntaleAlDocPaciente
//
//  Created by Osvaldo Fernández on 14/05/21.
//

import Foundation

struct ObtenerTarjetasResponse: Codable {
    let response: Bool
    let message: String?
    let tarjetas: [Tarjeta]?
}
