//
//  EspecialidadesReponse.swift
//  PreguntaleAlDocPaciente
//
//  Created by Osvaldo Fernández on 23/04/21.
//

import Foundation

struct EspecialidadesResponse: Codable {
    let response: Bool
    let especialidades: [Especialidad]
}
