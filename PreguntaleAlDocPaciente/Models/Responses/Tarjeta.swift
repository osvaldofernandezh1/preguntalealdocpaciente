//
//  Tarjeta.swift
//  PreguntaleAlDocPaciente
//
//  Created by Osvaldo Fernández on 14/05/21.
//

import Foundation

struct Tarjeta: Codable {
    let type: String
    let brand: String
    let bank_name: String
    let bank_code: String
    let id: String
    let card_number: String
    let holder_name: String
    let expiration_year: String
    let expiration_month: String
}
