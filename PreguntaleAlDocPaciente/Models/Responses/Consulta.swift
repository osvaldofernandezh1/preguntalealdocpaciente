//
//  Consulta.swift
//  PreguntaleAlDocPaciente
//
//  Created by Osvaldo Fernández on 14/05/21.
//

import Foundation

struct Consulta: Codable {
    let id_consulta: Int
    let calificacion: Int
    let observaciones: String?
    let nombre_doctor: String
    let foto_doctor: String?
    let nombre_paciente: String
    let fecha_cita: String
    let cita_pagada: Int
    let costo_consulta: Int
    let hora_cita: String
    let tiempo: Int
    let motivo: String
    let medicamentos: String
    let antecedentes: String
    let peso: Int
    let edad: Int
    let id_estatus_consulta: Int
    let desc_estatus_consulta: String
}
