//
//  CrearClienteResponse.swift
//  PreguntaleAlDocPaciente
//
//  Created by Osvaldo Fernández on 13/05/21.
//

import Foundation

struct CrearClienteResponse: Codable {
    let response: Bool
    let idopenpay: String
}
