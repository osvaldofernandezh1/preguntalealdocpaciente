//
//  HorariosResponse.swift
//  PreguntaleAlDocPaciente
//
//  Created by Osvaldo Fernández on 14/05/21.
//

import Foundation

struct HorariosResponse: Codable {
    let response: Bool
    let message: String?
    let horarios: [HorariosResponse]?
}
