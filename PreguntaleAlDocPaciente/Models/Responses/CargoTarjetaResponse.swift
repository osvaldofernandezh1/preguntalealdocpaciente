//
//  CargoTarjetaResponse.swift
//  PreguntaleAlDocPaciente
//
//  Created by Osvaldo Fernández on 14/05/21.
//

import Foundation

struct CargoTarjetaResponse: Codable {
    let response: Bool
    let message: String?
    let authorization: String?
    let id: String?
    let status: String?
}
