//
//  CreateOpenPayClientResponse.swift
//  PreguntaleAlDocPaciente
//
//  Created by Osvaldo Fernández on 09/05/21.
//

import Foundation

struct CreateOpenPayClientResponse: Codable {
    let id: String
    let name: String
    let last_name: String
    let email: String
    let phone_number: String
    let address: String
    let creation_date: String
    let external_id: String
    let store: ClientStore
    let clabe: String
}

