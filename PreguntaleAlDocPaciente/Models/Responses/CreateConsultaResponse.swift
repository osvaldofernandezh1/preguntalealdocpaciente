//
//  CreateConsultaResponse.swift
//  PreguntaleAlDocPaciente
//
//  Created by Osvaldo Fernández on 14/05/21.
//

import Foundation

struct CreateConsultaResponse: Codable {
    let response: Bool
    let message: String?
    let id_consulta: Int?
}
