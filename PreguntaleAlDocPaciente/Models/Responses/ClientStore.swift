//
//  ClientStore.swift
//  PreguntaleAlDocPaciente
//
//  Created by Osvaldo Fernández on 09/05/21.
//

import Foundation

struct ClientStore: Codable {
    let reference: String
    let barcode_url: String
}
