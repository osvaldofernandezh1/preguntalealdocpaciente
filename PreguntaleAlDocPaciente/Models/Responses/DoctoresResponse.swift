//
//  DoctoresResponse.swift
//  PreguntaleAlDocPaciente
//
//  Created by Osvaldo Fernández on 23/04/21.
//

import Foundation

struct DoctoresResponse: Codable {
    let response: Bool
    let doctores: [Doctor]
}
