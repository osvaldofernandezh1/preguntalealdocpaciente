//
//  LoginResponse.swift
//  PreguntaleAlDocPaciente
//
//  Created by Osvaldo Fernández on 19/04/21.
//

import Foundation

struct LoginResponse: Codable {
    let response: Bool
    let id_usuario: Int?
    let idopenpay: String?
    let nombre_usuario: String?
    let foto_usuario: String?
    let message: String?
}
