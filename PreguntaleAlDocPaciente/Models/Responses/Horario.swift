//
//  Horario.swift
//  PreguntaleAlDocPaciente
//
//  Created by Osvaldo Fernández on 14/05/21.
//

import Foundation

struct Horario: Codable {
    let id_horario: Int
    let hora_inicio: String
    let hora_fin: String
}
