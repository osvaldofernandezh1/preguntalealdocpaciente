//
//  ListaConsultasResponse.swift
//  PreguntaleAlDocPaciente
//
//  Created by Osvaldo Fernández on 14/05/21.
//

import Foundation

struct ListaConsultasResponse: Codable {
    let response: Bool
    let message: String?
    let consultas: [Consulta]?
}
