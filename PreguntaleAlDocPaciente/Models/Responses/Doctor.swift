//
//  Doctor.swift
//  PreguntaleAlDocPaciente
//
//  Created by Osvaldo Fernández on 23/04/21.
//

import Foundation

struct Doctor: Codable {
    let id_doctor: Int
    let costo_consulta: Int
    let foto_doctor: String?
    let nombre_doctor: String
    let apellidos_doctor: String
    let nombre_especialidad: String
    let rating: Int
    let acerca: String
    let num_citas: Int
}
