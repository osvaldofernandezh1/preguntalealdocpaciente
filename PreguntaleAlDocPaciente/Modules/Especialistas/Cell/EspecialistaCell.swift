//
//  EspecialistaCell.swift
//  PreguntaleAlDocPaciente
//
//  Created by Osvaldo Fernández on 23/04/21.
//

import Foundation
import UIKit

class EspecialistaCell: UICollectionViewCell {
    
    @IBOutlet weak var especialistaNombreLabel: UILabel!
    @IBOutlet weak var especialistaImageView: UIImageView!
    @IBOutlet weak var especialidadLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentView.layer.cornerRadius = 20
        self.contentView.clipsToBounds = true
        self.clipsToBounds = true
        self.layer.masksToBounds = false
        self.layer.cornerRadius = 20
        //ED7D6A
        //EBECEF
        let initialColor = #colorLiteral(red: 0.9294117647, green: 0.4901960784, blue: 0.4156862745, alpha: 1)
        let finalColor = #colorLiteral(red: 0.9215686275, green: 0.9254901961, blue: 0.937254902, alpha: 1)
        let gradientLayer = CAGradientLayer()
        gradientLayer.startPoint = CGPoint(x: 0, y: 1)
        gradientLayer.endPoint = CGPoint(x: 1, y: 1)
        gradientLayer.frame = self.contentView.bounds
        gradientLayer.colors = [initialColor.cgColor, finalColor.cgColor]
        self.contentView.layer.insertSublayer(gradientLayer, at: 0)
    }
}
