//
//  EspecialidtasViewController.swift
//  PreguntaleAlDocPaciente
//
//  Created by Osvaldo Fernández on 23/04/21.
//

import Foundation
import UIKit
import Alamofire
import AlamofireImage
import SwiftyGif

class EspecialistasViewController: UIViewController, UICollectionViewDataSource {
    
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    @IBOutlet weak var gifView: UIImageView!
    @IBOutlet weak var especialidadLabel: UILabel!
    @IBOutlet weak var especialistasCollectionView: UICollectionView!
    let especialidadId: String! = UserDefaults.standard.string(forKey: "currentEspecialidadId")
    let especialidadNombre: String! = UserDefaults.standard.string(forKey: "nombreEspecialidad")
    var doctoresDataSource: [Doctor] = []
    var currentDoctor: Doctor?
    override func viewDidLoad() {
        super.viewDidLoad()
        do {
            let gif = try UIImage(gifName: "animacion.gif", levelOfIntegrity: 0.5)
            self.gifView.setGifImage(gif, loopCount: -1)
            } catch {
                print(error)
               }
        especialidadLabel.text = especialidadNombre
        especialistasCollectionView.dataSource = self
        getDoctores()
        
    }
    
    func getDoctores() {
        self.loadingIndicator.isHidden = false
        AF.request(Endpoints.getDoctores + especialidadId).response { response in
            self.loadingIndicator.isHidden = true
            do {
                let decoded = try JSONDecoder().decode(DoctoresResponse.self, from: response.data!)
                print(decoded)
            } catch {
                print(error)
            }
            guard
                let dataFromService = response.data,
                let res = try? JSONDecoder().decode(DoctoresResponse.self, from: dataFromService) else {
                    print("decoding error")
                        return
                }
            self.doctoresDataSource = res.doctores
            self.especialistasCollectionView.reloadData()
        }
    }
    
    @objc func openDoctor(sender: UITapGestureRecognizer) {
        self.currentDoctor = doctoresDataSource[sender.view!.tag]
        performSegue(withIdentifier: "doctor_segue", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "doctor_segue" {
            if #available(iOS 13.0, *) {
                let vc = segue.destination as? DoctorViewController
                vc?.currentDoctor = self.currentDoctor
            } else {
                // Fallback on earlier versions
            }
                }
    }
    
    
    // MARK: - CollectionView Methods
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return doctoresDataSource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = especialistasCollectionView.dequeueReusableCell(withReuseIdentifier: "EspecialistaCell", for: indexPath) as! EspecialistaCell
        if let url = doctoresDataSource[indexPath.row].foto_doctor {
        AF.request(url).responseImage { response in
                                       if case .success(let image) = response.result {
                                          // print("image downloaded: \(image)")
                                        cell.especialistaImageView.image = image
                                       }
                                   }
                        }
        cell.especialistaNombreLabel.text = doctoresDataSource[indexPath.row].nombre_doctor.capitalized
        cell.especialidadLabel.text = doctoresDataSource[indexPath.row].nombre_especialidad.capitalized
        cell.contentView.tag = indexPath.row
        cell.contentView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.openDoctor)))
        return cell
    }
    
}
