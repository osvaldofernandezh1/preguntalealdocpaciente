//
//  DoctorViewController.swift
//  PreguntaleAlDocPaciente
//
//  Created by Osvaldo Fernández on 24/04/21.
//

import Foundation
import UIKit
import Alamofire
import AlamofireImage

@available(iOS 13.0, *)
class DoctorViewController: UIViewController {
    
    @IBOutlet weak var doctorImageView: UIImageView!
    @IBOutlet weak var doctorView: UIView!
    @IBOutlet weak var doctorDataView: UIView!
    @IBOutlet weak var doctorNameLabel: UILabel!
    @IBOutlet weak var doctorEspecialidadLabel: UILabel!
    @IBOutlet weak var starRating: StarRatingView!
    @IBOutlet weak var pacientesAtendidosLabel: UILabel!
    @IBOutlet weak var descripcionLabel: UILabel!
    @IBOutlet weak var citaButton: UIButton!
    
    var currentDoctor: Doctor?

    override func viewDidLoad() {
        super.viewDidLoad()
        let initialColor = #colorLiteral(red: 0.9294117647, green: 0.4901960784, blue: 0.4156862745, alpha: 1)
        let finalColor = #colorLiteral(red: 0.9215686275, green: 0.9254901961, blue: 0.937254902, alpha: 1)
        let gradientLayer = CAGradientLayer()
        gradientLayer.startPoint = CGPoint(x: 0, y: 1)
        gradientLayer.endPoint = CGPoint(x: 1, y: 1)
        gradientLayer.frame = self.doctorView.bounds
        gradientLayer.colors = [initialColor.cgColor, finalColor.cgColor]
        self.doctorView.layer.insertSublayer(gradientLayer, at: 0)
        self.doctorDataView.layer.cornerRadius = 22
        if let url = self.currentDoctor!.foto_doctor {
        AF.request(url).responseImage { response in
                        if case .success(let image) = response.result {
                                          // print("image downloaded: \(image)")
                            self.doctorImageView.image = image
                            }
                    }
            }
        doctorNameLabel.text = "Médico. " + self.currentDoctor!.nombre_doctor + " " + self.currentDoctor!.apellidos_doctor
        doctorEspecialidadLabel.text = self.currentDoctor!.nombre_especialidad
        self.starRating.rating = Float(self.currentDoctor!.rating)
        pacientesAtendidosLabel.text = "\(self.currentDoctor!.num_citas) pacientes atendidos"
        descripcionLabel.text = self.currentDoctor!.acerca
        citaButton.setTitle("SOLICITAR CITA $\(self.currentDoctor!.costo_consulta) MXN", for: .normal)
        citaButton.layer.cornerRadius = 20
    }
    
    @IBAction func citasButtonAction(_ sender: Any) {
        
    }
}
