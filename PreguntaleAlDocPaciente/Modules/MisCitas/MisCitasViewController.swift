//
//  MisCitasViewController.swift
//  PreguntaleAlDocPaciente
//
//  Created by Osvaldo Fernández on 19/04/21.
//

import Foundation
import UIKit
import Alamofire

class MisCitasViewController: UIViewController, UITableViewDataSource {
    var citasDataSource: [Consulta] = []
    @IBOutlet weak var sinCitasLabel: UILabel!
    @IBOutlet weak var citasTableView: UITableView!
    let idUsuario: String = UserDefaults.standard.string(forKey: "id_usuario")!
    
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        citasTableView.dataSource = self
        getCitas()
    }
    
    func getCitas() {
        let request = ListaConsultasRequest(action: "get_consultas", id_usuario: self.idUsuario)
        AF.request(Endpoints.serverUrl, method: .post, parameters: request.dictionaryRepresentation, encoder: URLEncodedFormParameterEncoder(destination: .queryString)).response { response in
            
            guard let data = response.data,
              let res = try? JSONDecoder().decode(ListaConsultasResponse.self, from: data) else {
                  print("error decoding")
                  return
              }
            if res.response == true {
                self.citasTableView.isHidden = false
                self.citasDataSource = res.consultas!
                self.citasTableView.reloadData()
            } else {
                self.citasTableView.isHidden = true
            }
        }
    }
    
    
    // MARK: - TableView Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return citasDataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.citasTableView.dequeueReusableCell(withIdentifier: "CitaCell", for: indexPath) as! CitaTableViewCell
        if let url = citasDataSource[indexPath.row].foto_doctor {
            AF.request(url).responseImage { response in
                                           if case .success(let image) = response.result {
                                              // print("image downloaded: \(image)")
                                            cell.citaImageView.image = image
                                           }
                                       }
        }
       
                        
        return cell
    }
    
}
