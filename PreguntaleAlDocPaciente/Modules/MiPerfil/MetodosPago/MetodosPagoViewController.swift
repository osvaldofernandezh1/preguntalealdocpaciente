//
//  MetodosPagoViewController.swift
//  PreguntaleAlDocPaciente
//
//  Created by Osvaldo Fernández on 03/05/21.
//

import Foundation
import UIKit
import OpenpayKit
import DropDown


class MetodosPagoViewController: UIViewController {

    @IBOutlet weak var dayView: UIView!
    let dayDropDown = DropDown()
    
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        dayView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.openDayDropDown)))
        dayDropDown.anchorView = dayView
        dayDropDown.dataSource = ["1", "2"]
    }
    
    
    @objc func openDayDropDown() {
        dayDropDown.show()
    }
    
    
}
