//
//  MiPerfilViewController.swift
//  PreguntaleAlDocPaciente
//
//  Created by Osvaldo Fernández on 19/04/21.
//

import Foundation
import UIKit

class MiPerfilViewController: UIViewController, UITableViewDataSource {
    
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var menuTableView: UITableView!
    let username = UserDefaults.standard.string(forKey: "username")
    var menuDataSource: [PerfilMenuOption] = [PerfilMenuOption(image: "ic_terminos", title: "Términos y condiciones", navigation: "TERMINOS"), PerfilMenuOption(image: "ic_politica", title: "Políticas de privacidad", navigation: "PRIVACIDAD"), PerfilMenuOption(image: "ic_calificar", title: "Valorar la aplicación", navigation: "CALIFICAR"), PerfilMenuOption(image: "metodos3", title: "Métodos de Pago", navigation: "PAGO"), PerfilMenuOption(image: "ic_logout", title: "Cerrar sesión", navigation: "LOGOUT")]
   
    override func viewDidLoad() {
        super.viewDidLoad()
        usernameLabel.text = username
        menuTableView.dataSource = self
    }
    
    @objc func clickOption(sender: UITapGestureRecognizer) {
           let tapped = sender.view!
           let navigationOption = self.menuDataSource[tapped.tag].navigation!
        
        switch navigationOption {
        case "PAGO":
            self.performSegue(withIdentifier: "pago_segue", sender: nil)
            
        default:
            print("")
        }
       }
    
    
    // MARK: - TableView Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuDataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MenuCell", for: indexPath) as! PerfilMenuTableViewCell
        cell.optionImageView.image = UIImage(named: menuDataSource[indexPath.row].image)
        cell.optionNameLabel.text = menuDataSource[indexPath.row].title
        cell.contentView.tag = indexPath.row
        cell.contentView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.clickOption)))
        
        return cell
    }
    
}
