//
//  PerfilMenuTableViewCell.swift
//  PreguntaleAlDocPaciente
//
//  Created by Osvaldo Fernández on 19/04/21.
//

import Foundation
import UIKit

class PerfilMenuTableViewCell: UITableViewCell {
    @IBOutlet weak var optionImageView: UIImageView!
    @IBOutlet weak var optionNameLabel: UILabel!
    
}
