//
//  DireccionPagoViewController.swift
//  PreguntaleAlDocPaciente
//
//  Created by Osvaldo Fernández on 03/05/21.
//

import Foundation
import UIKit
import Alamofire

class DireccionPagoViewController: UIViewController {
    
    @IBOutlet weak var calleTextField: UITextField!
    @IBOutlet weak var coloniaTextField: UITextField!
    @IBOutlet weak var estadoTextField: UITextField!
    @IBOutlet weak var ciudadTextField: UITextField!
    @IBOutlet weak var cpTextField: UITextField!
    @IBOutlet weak var registerButton: UIButton!
    let idUsuario = UserDefaults.standard.integer(forKey: "id_usuario")
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    // MARK: - IB Methods
    @IBAction func registerButtonAction(_ sender: Any) {
        registerClient()
    }
    
    
    // MARK: - Methods
    func registerClient() {
        let request = CrearClienteRequest(action: "crear_cliente", id_usuario: "\(self.idUsuario)", calle: self.calleTextField.text!, colonia: self.coloniaTextField.text!, ciudad: self.ciudadTextField.text!, estado: self.estadoTextField.text!, codigo_postal: self.cpTextField.text!)
        AF.request(Endpoints.openPay, method: .post, parameters: request.dictionaryRepresentation, encoding: URLEncoding.default).response { response in
            guard let data = response.data,
              let res = try? JSONDecoder().decode(CrearClienteResponse.self, from: data) else {
                  print("error decoding")
                  return
              }
            UserDefaults.standard.setValue(res.idopenpay, forKey: "id_openpay")
        }
    }
}
