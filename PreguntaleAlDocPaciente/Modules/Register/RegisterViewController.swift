//
//  RegisterViewController.swift
//  PreguntaleAlDocPaciente
//
//  Created by Osvaldo Fernández on 02/05/21.
//

import Foundation
import UIKit

class RegisterViewController: UIViewController {
    
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    @IBOutlet weak var registerButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        nameTextField.layer.cornerRadius = 20
        nameTextField.setPadding()
        
        emailTextField.layer.cornerRadius = 20
        emailTextField.setPadding()
        
        phoneTextField.layer.cornerRadius = 20
        phoneTextField.setPadding()
        
        passwordTextField.layer.cornerRadius = 20
        passwordTextField.setPadding()
        
        confirmPasswordTextField.layer.cornerRadius = 20
        confirmPasswordTextField.setPadding()
        
        
    }
    
    @IBAction func registerButtonAction(_ sender: Any) {
        
    }

}
