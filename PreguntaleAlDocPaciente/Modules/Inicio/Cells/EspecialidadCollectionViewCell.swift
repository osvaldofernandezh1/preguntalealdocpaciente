//
//  EspecialidadCollectionViewCell.swift
//  PreguntaleAlDocPaciente
//
//  Created by Osvaldo Fernández on 23/04/21.
//

import Foundation
import UIKit

class EspecialidadCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var especialidadView: UIView!
    @IBOutlet weak var especialidadLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        especialidadView.layer.cornerRadius = 16
    }
}
