//
//  InicioViewController.swift
//  PreguntaleAlDocPaciente
//
//  Created by Osvaldo Fernández on 19/04/21.
//

import Foundation
import UIKit
import Alamofire

class InicioViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var especialidadesCollectionView: UICollectionView!
    @IBOutlet weak var welcomeLabel: UILabel!
    let username = UserDefaults.standard.string(forKey: "username")
    var especialidadesDataSource: [Especialidad] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        welcomeLabel.text = ("Hola, \(username!)").uppercased()
        especialidadesCollectionView.dataSource = self
        especialidadesCollectionView.delegate = self
        getEspecialidades()
    }
    
    func getEspecialidades() {
        AF.request(Endpoints.getEspecialidades).response { response in
            guard
                let dataFromService = response.data,
                let res = try? JSONDecoder().decode(EspecialidadesResponse.self, from: dataFromService) else {
                    print("decoding error")
                        return
                }
            self.especialidadesDataSource = res.especialidades
            self.especialidadesDataSource.append(Especialidad(id_especialidad: 0, nombre_especialidad: "Ver más"))
            self.especialidadesCollectionView.reloadData()
        }
    }
    
    @objc func openEspecialidad(sender: UITapGestureRecognizer) {
        print(sender.view!.tag)
        let nombreEspecialidad = self.especialidadesDataSource.first(where: {$0.id_especialidad == sender.view!.tag})
        UserDefaults.standard.set(nombreEspecialidad!.nombre_especialidad, forKey: "nombreEspecialidad")
        UserDefaults.standard.set(sender.view!.tag, forKey: "currentEspecialidadId")
        
        performSegue(withIdentifier: "especialistas_segue", sender: nil)
    }
    
    
    // MARK: - CollectionView Methods
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return especialidadesDataSource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = especialidadesCollectionView.dequeueReusableCell(withReuseIdentifier: "EspecialidadCell", for: indexPath) as! EspecialidadCollectionViewCell
        cell.especialidadLabel.text = especialidadesDataSource[indexPath.row].nombre_especialidad
        cell.especialidadView.tag = especialidadesDataSource[indexPath.row].id_especialidad
        cell.especialidadView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.openEspecialidad)))
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
                let numberOfItemsPerRow:CGFloat = 3
                let spacingBetweenCells:CGFloat = 0
                let spacing = 1
            let totalSpacing = (2 * CGFloat(spacing)) + ((numberOfItemsPerRow - 1) * spacingBetweenCells) //Amount of total spacing in a row
                let collection = self.especialidadesCollectionView
                let width = (collection!.bounds.width - totalSpacing)/numberOfItemsPerRow
                    return CGSize(width: width, height: 30)
            
    }
}
