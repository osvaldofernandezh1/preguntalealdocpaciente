//
//  LoginViewController.swift
//  PreguntaleAlDocPaciente
//
//  Created by Osvaldo Fernández on 17/04/21.
//

import Foundation
import UIKit
import Alamofire

class LoginViewController: UIViewController {
    @IBOutlet weak var celularTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var registerLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        celularTextField.layer.cornerRadius = 20
        celularTextField.setPadding()
        
        passwordTextField.layer.cornerRadius = 20
        passwordTextField.setPadding()
        
        loginButton.layer.cornerRadius = 20
        
        registerLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.openRegister)))
        
        
    }
    
    @IBAction func loginButtonAction(_ sender: Any) {
        loginRequest()
    }
    
    
    @objc func openRegister(sender: UITapGestureRecognizer) {
        print("open register")
        self.performSegue(withIdentifier: "register_segue", sender: nil)
    }
    
    func loginRequest() {
        let cellPhoneNumber = self.celularTextField.text!
        let request = LoginRequest(celular_usuario: cellPhoneNumber, device_token: "", password_usuario: self.passwordTextField.text!, id_tipo_dispositivo: "2")
        
        
        AF.request(Endpoints.login, method: .post, parameters: request.dictionaryRepresentation, encoding: URLEncoding.default).response { response in
            print(response)
            
            switch response.result {
            case .success:
                guard let data = response.data,
                  let res = try? JSONDecoder().decode(LoginResponse.self, from: data) else {
                      print("error decoding")
                      return
                  }
                if res.response == true {
                    UserDefaults.standard.setValue(res.nombre_usuario!, forKey: "username")
                    UserDefaults.standard.setValue(res.id_usuario, forKey: "id_usuario")
                    self.performSegue(withIdentifier: "home_segue", sender: nil)
                }
            case .failure:
                print("Error")
            }
        }
    }
}
