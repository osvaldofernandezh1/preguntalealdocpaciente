//
//  Endpoints.swift
//  PreguntaleAlDocPaciente
//
//  Created by Osvaldo Fernández on 17/04/21.
//

import Foundation

struct Endpoints {
    static let serverUrl = "http://unidadmedicaintegral.venturapp.com/dashboard/api_usuarios/?"
    
    static let login = Endpoints.serverUrl + "action=login"
    static let getEspecialidades = Endpoints.serverUrl + "action=get_especialidades"
    static let getDoctores = Endpoints.serverUrl + "action=get_doctores&id_especialidad="
    static let openPay = "http://unidadmedicaintegral.venturapp.com/dashboard/controllers/openpay/api_openpay.php?"
}
