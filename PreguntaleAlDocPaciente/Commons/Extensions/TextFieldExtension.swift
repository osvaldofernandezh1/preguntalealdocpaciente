//
//  TextFieldExtension.swift
//  PreguntaleAlDocPaciente
//
//  Created by Osvaldo Fernández on 19/04/21.
//

import Foundation
import UIKit

extension UITextField {
    
    func setPadding() {
        let leftView = UIView(frame: CGRect(x: 0.0, y: 0.0, width: 10.0, height: 2.0))
        self.leftView = leftView
        self.leftViewMode = .always
    }
    
}
